package com.fabrikam;

import com.azure.storage.blob.BlobClient;
import com.azure.storage.blob.BlobServiceClient;
import com.azure.storage.blob.BlobServiceClientBuilder;
import com.microsoft.azure.functions.ExecutionContext;
import com.microsoft.azure.functions.HttpMethod;
import com.microsoft.azure.functions.HttpRequestMessage;
import com.microsoft.azure.functions.HttpResponseMessage;
import com.microsoft.azure.functions.HttpStatus;
import com.microsoft.azure.functions.annotation.AuthorizationLevel;
import com.microsoft.azure.functions.annotation.FunctionName;
import com.microsoft.azure.functions.annotation.HttpTrigger;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.CloudBlob;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.util.List;
import java.util.Optional;

/**
 * Azure Functions with HTTP Trigger.
 */
public class Function {
    /**
     * This function listens at endpoint "/api/HttpExample". Two ways to invoke it using "curl" command in bash:
     * 1. curl -d "HTTP Body" {your host}/api/HttpExample
     * 2. curl "{your host}/api/HttpExample?name=HTTP%20Query"
     */
    static String connectionString = "DefaultEndpointsProtocol=https;AccountName=mysummerstorage;AccountKey=70GPEvYQDBoYIM8b7M/sf0rnL9t5WR/bF1PMKs1/oPjcQDOtiJT56G4kHkzQWy6qsBDn13OiNKJPea1d5cS08w==;EndpointSuffix=core.windows.net";

    @FunctionName("DownloadPage")
    public HttpResponseMessage run(
            @HttpTrigger(
                    name = "req",
                    methods = {HttpMethod.GET, HttpMethod.POST},
                    authLevel = AuthorizationLevel.ANONYMOUS)
                    HttpRequestMessage<Optional<String>> request,
            final ExecutionContext context) throws URISyntaxException, InvalidKeyException, StorageException {
        context.getLogger().info("Java HTTP trigger processed a request.");

        // Parse query parameter
        //final String query = request.getQueryParameters().get("file");
        String name = request.getQueryParameters().get("file");
        if (name == null)
            name = "lock.txt";
        CloudStorageAccount cloudStorageAccount = CloudStorageAccount.parse(connectionString);
        CloudBlobClient cloudBlobClient = cloudStorageAccount.createCloudBlobClient();
        CloudBlobContainer container = cloudBlobClient.getContainerReference("files");
        String filename = container.getBlobReferenceFromServer(name).getMetadata().get("filename");
        String s = "<html>\n" +
                "<head>\n" +
                "<title>Upload to Cloud</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<h1>Click to download file</h1>\n" +
                "<form method=\"post\" enctype=\"multipart/form-data\" action=\"https://func4-20200721003356798.azurewebsites.net/api/Downloader?file=" + name + "\">\n" +
                " <div>\n" +
                "   <label>" + filename + "</label>\n" +
                " </div>\n" +
                " <div>\n" +
                "   <button>Download</button>\n" +
                " </div>\n" +
                "</form>\n" +
                "</body>\n" +
                "</html>";

        return request.createResponseBuilder(HttpStatus.OK)
                .header("Content-Type", "text/html")
                .header("charset", "UTF-8")
                .body(s).build();
    }
}
