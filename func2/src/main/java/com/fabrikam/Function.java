package com.fabrikam;

import com.azure.core.implementation.serializer.jsonwrapper.spi.JsonPlugin;
import com.microsoft.azure.functions.ExecutionContext;
import com.microsoft.azure.functions.HttpMethod;
import com.microsoft.azure.functions.HttpRequestMessage;
import com.microsoft.azure.functions.HttpResponseMessage;
import com.microsoft.azure.functions.HttpStatus;
import com.microsoft.azure.functions.annotation.AuthorizationLevel;
import com.microsoft.azure.functions.annotation.FunctionName;
import com.microsoft.azure.functions.annotation.HttpTrigger;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.HashMap;
import java.util.Optional;
import java.util.UUID;

/**
 * Azure Functions with HTTP Trigger.
 */
public class Function {
    /**
     * This function listens at endpoint "/api/HttpExample". Two ways to invoke it using "curl" command in bash:
     * 1. curl -d "HTTP Body" {your host}/api/HttpExample
     * 2. curl "{your host}/api/HttpExample?name=HTTP%20Query"
     */

    static String connectionString = "DefaultEndpointsProtocol=https;AccountName=mysummerstorage;AccountKey=70GPEvYQDBoYIM8b7M/sf0rnL9t5WR/bF1PMKs1/oPjcQDOtiJT56G4kHkzQWy6qsBDn13OiNKJPea1d5cS08w==;EndpointSuffix=core.windows.net";

    @FunctionName("Uploader")
    public HttpResponseMessage run(
            @HttpTrigger(
                    name = "req",
                    methods = {HttpMethod.GET, HttpMethod.POST},
                    authLevel = AuthorizationLevel.ANONYMOUS)
                    HttpRequestMessage<Optional<String>> request,
            final ExecutionContext context) throws URISyntaxException, InvalidKeyException, StorageException, IOException {
        context.getLogger().info("Java HTTP trigger processed a request.");

        String fileName = "name", fileContentType = "", fileBody = "";
        String uuidName = UUID.randomUUID().toString();
        String req = request.getBody().toString();
        String[] a = req.split("\r\n");

        //BODY
        StringBuilder content = new StringBuilder();
        for (int i = 4; i < a.length - 2; i++)
            content.append(a[i]);

        fileBody = content.toString();

        String[] str = req.split("\r\n");

        //TYPE
        String data = "";
        String name = "";
        int k = 0;
        int id = 0;

        for (String string : str) {
            data += k + ") " + string;
            k++;
            if (string.startsWith("Content-Type:"))
                fileContentType = string;
        }

        int space = 0;
        for (int i = 0; i < fileContentType.length(); i++)
            if (fileContentType.charAt(i) == ' ') {
                space = i;
            }

        if (space < fileContentType.length()) {
            String contentType = fileContentType;
            fileContentType = "";
            for (int i = space + 1; i < contentType.length(); i++)
                fileContentType += contentType.charAt(i);
        }

        //FILENAME
        for (String string : str) {
            for (int i = 0; i < string.length() - 7; i++) {
                name = "" + string.charAt(i) + string.charAt(i + 1) + string.charAt(i + 2) + string.charAt(i + 3) + string.charAt(i + 4)
                + string.charAt(i + 5) + string.charAt(i + 6) + string.charAt(i + 7);
                if (name.equals("filename")) {
                    id = i + 10;
                    fileName = "";
                    for (int j = id; j < string.length() - 1; j++)
                        fileName += string.charAt(j);
                }
            }
        }

        if (fileName.length() == 0)
            fileName = "name";

        String length = request.getHeaders().get("content-length");
        HashMap<String, String> metadata = new HashMap<>();
        metadata.put("filename", fileName);
        InputStream is = new ByteArrayInputStream(fileBody.getBytes());

        CloudStorageAccount cloudStorageAccount = CloudStorageAccount.parse(connectionString);
        CloudBlobClient cloudBlobClient = cloudStorageAccount.createCloudBlobClient();
        CloudBlobContainer container = cloudBlobClient.getContainerReference("files");
        CloudBlockBlob blob = container.getBlockBlobReference(uuidName);
        blob.setMetadata(metadata);
        blob.getProperties().setContentType(fileContentType);
        blob.upload(is, length == null ? 0 : Integer.parseInt(length));

        String s = "<html>" +
                "<head><title><h1>page2</h1></title></head>" +
                "<body>" +
                "<h4>Link to download</h4>" +
                "<br><p><a href=\"https://func3-20200720211106360.azurewebsites.net/api/DownloadPage?file=" + uuidName
                + "\">" + "https://func3-20200720211106360.azurewebsites.net/api/DownloadPage?file=" + uuidName +
                "</a></p>" + "\n" +
                "</body>" +
                "</html>";

        return request.createResponseBuilder(HttpStatus.OK)
                .header("Content-Type", "text/html")
                .header("charset", "UTF-8")
                .body(s)
                .build();
    }
}
