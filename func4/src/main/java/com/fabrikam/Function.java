package com.fabrikam;

import com.microsoft.azure.functions.ExecutionContext;
import com.microsoft.azure.functions.HttpMethod;
import com.microsoft.azure.functions.HttpRequestMessage;
import com.microsoft.azure.functions.HttpResponseMessage;
import com.microsoft.azure.functions.HttpStatus;
import com.microsoft.azure.functions.annotation.AuthorizationLevel;
import com.microsoft.azure.functions.annotation.BlobInput;
import com.microsoft.azure.functions.annotation.FunctionName;
import com.microsoft.azure.functions.annotation.HttpTrigger;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.BlobInputStream;
import com.microsoft.azure.storage.blob.CloudBlob;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;

import java.io.*;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.Optional;

/**
 * Azure Functions with HTTP Trigger.
 */
public class Function {

    static String connectionString = "DefaultEndpointsProtocol=https;AccountName=mysummerstorage;AccountKey=70GPEvYQDBoYIM8b7M/sf0rnL9t5WR/bF1PMKs1/oPjcQDOtiJT56G4kHkzQWy6qsBDn13OiNKJPea1d5cS08w==;EndpointSuffix=core.windows.net";
    /**
     * This function listens at endpoint "/api/HttpExample". Two ways to invoke it using "curl" command in bash:
     * 1. curl -d "HTTP Body" {your host}/api/HttpExample
     * 2. curl "{your host}/api/HttpExample?name=HTTP%20Query"
     */
    @FunctionName("Downloader")
    public HttpResponseMessage run(
            @HttpTrigger(
                name = "req",
                methods = {HttpMethod.GET, HttpMethod.POST},
                authLevel = AuthorizationLevel.ANONYMOUS)
                HttpRequestMessage<Optional<String>> request,
            final ExecutionContext context) throws URISyntaxException, StorageException, InvalidKeyException, IOException {
        context.getLogger().info("Java HTTP trigger processed a request.");

        // Parse query parameter
        String name = request.getQueryParameters().get("file");
        if (name == null)
            name = "lock.txt";
        CloudStorageAccount cloudStorageAccount = CloudStorageAccount.parse(connectionString);
        CloudBlobClient cloudBlobClient = cloudStorageAccount.createCloudBlobClient();
        CloudBlobContainer container = cloudBlobClient.getContainerReference("files");
        CloudBlob blob = container.getBlobReferenceFromServer(name);
        String type = blob.getProperties().getContentType();
        String fileName = blob.getMetadata().get("filename");
        StringBuilder body = new StringBuilder();
        BlobInputStream blobInputStream = blob.openInputStream();
        int chr;
        while ((chr = blobInputStream.read()) != -1) {
            body.append((char)chr);
        }
        blobInputStream.close();
        blob.delete();

        return request.createResponseBuilder(HttpStatus.OK)
                .header("Content-Type", type)
                .header("Content-Disposition", "attachment; filename=" + fileName)
                .body(body.toString()).build();
    }
}
