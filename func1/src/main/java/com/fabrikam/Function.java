package com.fabrikam;

import com.microsoft.azure.functions.ExecutionContext;
import com.microsoft.azure.functions.HttpMethod;
import com.microsoft.azure.functions.HttpRequestMessage;
import com.microsoft.azure.functions.HttpResponseMessage;
import com.microsoft.azure.functions.HttpStatus;
import com.microsoft.azure.functions.annotation.AuthorizationLevel;
import com.microsoft.azure.functions.annotation.FunctionName;
import com.microsoft.azure.functions.annotation.HttpTrigger;

import java.util.Optional;

/**
 * Azure Functions with HTTP Trigger.
 */
public class Function {
    /**
     * This function listens at endpoint "/api/HttpExample". Two ways to invoke it using "curl" command in bash:
     * 1. curl -d "HTTP Body" {your host}/api/HttpExample
     * 2. curl "{your host}/api/HttpExample?name=HTTP%20Query"
     */
    @FunctionName("StartPage")
    public HttpResponseMessage run(
            @HttpTrigger(
                name = "req",
                methods = {HttpMethod.GET, HttpMethod.POST},
                authLevel = AuthorizationLevel.ANONYMOUS)
                HttpRequestMessage<Optional<String>> request,
            final ExecutionContext context) {
        context.getLogger().info("Java HTTP trigger processed a request.");

        // Parse query parameter
        final String query = request.getQueryParameters().get("name");
        final String name = request.getBody().orElse(query);
        String s = "<html>\n" +
                "<head>\n" +
                "<title>Upload to Cloud</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<h1>Upload to Cloud</h1>\n" +
                "<form method=\"post\" enctype=\"multipart/form-data\" action=\"https://func1-20200718121951257.azurewebsites.net/api/Uploader\">\n" +
                " <div>\n" +
                "   <label for=\"file\">Choose file to upload</label>\n" +
                "   <input name=\"InputFile\" type=\"file\">\n" +
                " </div>\n" +
                " <div>\n" +
                "   <button>Upload</button>\n" +
                " </div>\n" +
                "</form>\n" +
                "</body>\n" +
                "</html>";

        return request.createResponseBuilder(HttpStatus.OK)
                .header("Content-Type", "text/html")
                .header("charset", "UTF-8")
                .body(s).build();
    }
}
